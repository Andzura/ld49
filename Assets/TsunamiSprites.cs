using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TsunamiSprites : MonoBehaviour
{
  public Sprite[] sprites;

    void Start()
    {
      gameObject.GetComponent<SpriteRenderer>().sprite = sprites[3];
    }


  public void SetSprite(float ratio)
  {
    int i = Mathf.FloorToInt(ratio * 10f) + 3;
    //if i ok
    gameObject.GetComponent<SpriteRenderer>().sprite = sprites[i];
  }

  public void SetIthSprite(int i)
  {
    gameObject.GetComponent<SpriteRenderer>().sprite = sprites[i];
  }

    // Update is called once per frame
    void Update()
    {
        
    }
}
