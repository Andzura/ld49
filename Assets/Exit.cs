using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape)){
              #if UNITY_STANDALONE
                  //Quit the application
                  Application.Quit();
              #endif
          
                  //If we are running in the editor
              #if UNITY_EDITOR
                  //Stop playing the scene
                  UnityEditor.EditorApplication.isPlaying = false;
              #endif
        }
    }
}
