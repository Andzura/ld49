using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCursor : MonoBehaviour
{
    public float inertia = 0.1f;
    public float speed = 1f;
    private Vector3 lastVelocity;
    public PlayerGravity player;

    void Awake(){
    }
    // Start is called before the first frame update
    void Start(){
        this.lastVelocity = new Vector3(); 
    }

    void FixedUpdate(){
        Vector3 temp = Input.mousePosition;
        temp.z = 0f;
        Vector3 to = Camera.main.ScreenToWorldPoint(temp);
        Vector3 direction = (to - transform.position);
        float dist = direction.magnitude;
        if(dist <= 1f){
            //TODO
        }else{
            direction.Normalize();
        }
        
        Vector3 vel = ((direction * speed)/Mathf.Log10(player.getMass())) + lastVelocity * inertia;
        transform.position += vel;
        lastVelocity = vel;
    }
    // Update is called once per frame
    void Update(){
        
    }
}
