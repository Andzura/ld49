using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class PlayerGravity : MonoBehaviour
{
    public float size = 0.1f;
    public float attractionForce = 0.1f;
    public float growthRate = 0.1f;
    // ((base_force * attractionForce) / distance) ?
    // Start is called before the first frame update
    public float horizon = 0.5f;
    private float mass = 10f;
    public float rotationSpeed = 0.001f;
    private List<Debris> capturedDebris;
    private List<Debris> throwawayDebris;
    public int throwawayRate = 10;
    public int maxDebris = 150;
    private int counter = 0;
    public PlayableDirector birthAnim;
    public DebrisSpawner spawner;
    public SpriteRenderer bodySprite;
    public SpriteRenderer armsSprite;
    public SpriteRenderer faceSprite;

    void Start()
    {
        capturedDebris = new List<Debris>();
        throwawayDebris = new List<Debris>();
        bodySprite.gameObject.SetActive(false);
        armsSprite.gameObject.SetActive(false);
        faceSprite.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        UpdateDebris();
        if(capturedDebris.Count> maxDebris)
        {
            throwawayRate = 0;
            spawner.rate = 0;
            birthAnim.Play();
        }
    }

    public float GetHorizon()
    {
        return horizon;
    }

    public float getMass()
    {
        return mass;
    }

    public void Inflate(Debris newDebris)
    {
        float growth = newDebris.getMass() * growthRate;
        //transform.localScale += new Vector3(growth*0.7f, growth*0.7f);
        horizon += growth*0.5f;
        size += growth;
        this.mass += newDebris.getMass();
        newDebris.targetPoint = transform.position - newDebris.transform.position;
        newDebris.birthDate = Time.frameCount;
        capturedDebris.Add(newDebris);
        if(capturedDebris.Count>maxDebris+1)
        {
            int rnd = Random.Range(0, maxDebris);
            Debris dyingDebris = capturedDebris[rnd];
            capturedDebris.RemoveAt(rnd);
            Destroy(dyingDebris.gameObject);
        }
    }


    void UpdateDebris()
    {
        if (capturedDebris.Count <= 0)
            return;
        foreach (Debris debris in capturedDebris)
        {
            debris.maxVelocity = 5f;
            debris.SetAcceleration(new Vector3(0f, 0f, 0f));

            Vector3 rotatedTarget = new Vector3(debris.targetPoint.x * Mathf.Cos((Time.frameCount - debris.birthDate) * rotationSpeed), debris.targetPoint.y * Mathf.Sin((Time.frameCount - debris.birthDate) * rotationSpeed), 0f);

            //alt version
            //float cos = Mathf.Cos((Time.frameCount - debris.birthDate) * rotationSpeed);
            //float sin = Mathf.Sin((Time.frameCount - debris.birthDate) * rotationSpeed);
            //Vector3 rotatedTarget = new Vector3(debris.targetPoint.x * cos - debris.targetPoint.y * sin, debris.targetPoint.x * sin + debris.targetPoint.y * cos, 0f);

            debris.SetVelocity((transform.position + rotatedTarget - debris.transform.position) * 0.1f);
        }

        counter += Random.Range(0, throwawayRate);
        if (counter >= 100)
        {
            counter = 0;
            int rnd = Random.Range(0, capturedDebris.Count);
            Debris thrownDebris = capturedDebris[rnd];
            capturedDebris.RemoveAt(rnd);
        }
    }


    public void TriggerBirth()
    {
        if (capturedDebris.Count <= 0)
            return;
        foreach (Debris debris in capturedDebris)
        {
            debris.gameObject.SetActive(false);
        }
        bodySprite.gameObject.SetActive(true);
        armsSprite.gameObject.SetActive(true);
        faceSprite.gameObject.SetActive(true);
    }

  public void TriggerNextScene()
  {
    SceneManager.LoadScene("BasicScene", LoadSceneMode.Single);
  }

}
