using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;
using UnityEngine.UI;

public class RepartitionJauge : Jauge {


  public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
  public Sprite currentSprite;
  public float currentDelta;
  private float startY;
  private float floatSpan = 1f;
  private float speed  = 0.5f;

  public Image[] images;

  internal float forestPercent { get; set; }
  internal float mountainPercent { get; set; }
  internal float oceanPercent { get; set; }

  public SpriteRenderer renderer;

  public void Awake() {
    this.renderer = this.gameObject.GetComponent<SpriteRenderer>();
    renderer.transform.position = new Vector3(7, 2.3f, 0);
    startY = renderer.transform.position.y;

    this.pn1 = new PerlinNoiseGenerator(150f, 0.1f);
    this.pn2 = new PerlinNoiseGenerator(200f, 0.1f);

    this.sprites.Add("happy", Resources.Load<Sprite>("HUD/Repartition/happy"));
    this.sprites.Add("meh", Resources.Load<Sprite>("HUD/Repartition/meh"));
    this.sprites.Add("sad", Resources.Load<Sprite>("HUD/Repartition/sad"));
  }

  public void Update() {
    float t1 = pn1.generate(Time.deltaTime);
    float t2 = pn2.generate(Time.deltaTime);
    float x = 6.9f + 0.2f * t1;
    float y = 1.5f + 1.5f*t2;
    gameObject.transform.position = new Vector3(x, y, 0f);
  }

  public void SetDelta(float delta) {
    this.currentDelta = delta;
    SelectSprite();
  }

  public void SelectSprite() {
    if (currentDelta < 0.2f)
      this.currentSprite = this.sprites["happy"];
    else if (currentDelta >= 0.2f && currentDelta < 0.5f)
      this.currentSprite = this.sprites["meh"];
    else
      this.currentSprite = this.sprites["sad"];

    this.renderer.sprite = this.currentSprite;
  }

    internal void RefreshPieChart(){
      float value = oceanPercent;
      images[0].fillAmount = value;
      value += mountainPercent;
      images[1].fillAmount = value;
      images[2].fillAmount = value + forestPercent;
    }
}
