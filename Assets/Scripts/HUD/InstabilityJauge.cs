using System.Collections.Generic;
using UnityEngine;

public class InstabilityJauge : Jauge {


  public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
  public Sprite currentSprite;
  public int currentLevel;
  private float startY;
  private float floatSpan = 1f;
  private float speed  = 0.5f;


  public SpriteRenderer renderer;

  public void Awake() {
    this.renderer = this.gameObject.GetComponent<SpriteRenderer>();
    renderer.transform.position = new Vector3(-8, 2.5f, 0);
        startY = renderer.transform.position.y;

    this.pn1 = new PerlinNoiseGenerator(150f, 0.1f);
    this.pn2 = new PerlinNoiseGenerator(200f, 0.1f);
    this.sprites.Add("level0", Resources.Load<Sprite>("HUD/Instability/Jauge2-0"));
    this.sprites.Add("level1", Resources.Load<Sprite>("HUD/Instability/Jauge2-1"));
    this.sprites.Add("level2", Resources.Load<Sprite>("HUD/Instability/Jauge2-2"));
    this.sprites.Add("level3", Resources.Load<Sprite>("HUD/Instability/Jauge2-3"));
    this.sprites.Add("level4", Resources.Load<Sprite>("HUD/Instability/Jauge2-4"));
    this.sprites.Add("level5", Resources.Load<Sprite>("HUD/Instability/Jauge2-5"));
  }

  public void Update() {
    float t1 = pn1.generate(Time.deltaTime);
    float t2 = pn2.generate(Time.deltaTime);
    float x = -7.9f + 0.2f * t1;
    float y = 1f + 1.5f * t2;
    gameObject.transform.position = new Vector3(x, y, 0f);
  }

  public void SelectSprite() {
    this.currentSprite = this.sprites[$"level{currentLevel}"];
    this.renderer.sprite = this.currentSprite;
  }

  public void SetLevel(float level) {
    if (level < 0.2f)
      currentLevel = 0;
    else if (level >= 0.2f && level < 0.4f)
      currentLevel = 1;
    else if (level >= 0.4f && level < 0.6f)
      currentLevel = 2;
    else if (level >= 0.6f && level < 0.8f)
      currentLevel = 3;
    else if (level >= 0.8f && level < 1f)
      currentLevel = 4;
    else
      currentLevel = 5;

    SelectSprite();
  }
}
