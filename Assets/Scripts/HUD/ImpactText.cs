using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;
using UnityEngine.UI;
public class ImpactText : MonoBehaviour {

  void Update(){
    StabilityManager sm = Finder.FindManager();
    float timeleft = sm.gameTotalTime - sm.gameTimer;
    timeleft *= 100;
    GetComponent<Text>().text = "Impact in "+ (int)Mathf.Floor(timeleft)+ " Millions Years";
  }
}