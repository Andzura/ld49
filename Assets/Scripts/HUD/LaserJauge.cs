using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;

public class LaserJauge : Jauge {


  public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
  public Sprite currentSprite;
  public int currentHumanCount;
  public int requiredHumanCount;
  public int currentCityCount;
  public int requiredCityCount;
  public int currentStep = 0;


  public SpriteRenderer renderer;

  public void Awake() {
    this.renderer = this.gameObject.GetComponent<SpriteRenderer>();
    renderer.transform.position = new Vector3(3, -4, 0);

    this.pn1 = new PerlinNoiseGenerator(250f, 0.1f);
    this.pn2 = new PerlinNoiseGenerator(300f, 0.1f);
    this.sprites.Add("step1", Resources.Load<Sprite>("HUD/Laser/Laser-1"));
    this.sprites.Add("step2", Resources.Load<Sprite>("HUD/Laser/Laser-2"));
    this.sprites.Add("step3", Resources.Load<Sprite>("HUD/Laser/Laser-3"));
    this.sprites.Add("step4", Resources.Load<Sprite>("HUD/Laser/Laser-4"));
    this.sprites.Add("step5", Resources.Load<Sprite>("HUD/Laser/Laser-5"));
    this.sprites.Add("step6", Resources.Load<Sprite>("HUD/Laser/Laser-6"));
  }

  public void Update() {
    float t1 = pn1.generate(Time.deltaTime);
    float t2 = pn2.generate(Time.deltaTime);
    float x = 2.5f + t1;
    float y = -3.3f + t2;
    gameObject.transform.position = new Vector3(x, y, 0f);
  }

  public void SelectSprite() {
    this.currentSprite = this.sprites[$"step{currentStep}"];
    this.renderer.sprite = this.currentSprite;
  }

  public void SetCount(int humans, int cities) {
    Debug.Log(humans);
    float step = this.requiredHumanCount / 6;
    this.currentHumanCount = humans;
    this.currentCityCount = cities;

    if (currentHumanCount < step)
      currentStep = 0;
    else if (currentHumanCount >= step && currentHumanCount < 2*step)
      currentStep = 1;
    else if (currentHumanCount >= 2*step && currentHumanCount < 3*step)
      currentStep = 2;
    else if (currentHumanCount >= 3*step && currentHumanCount < 4*step)
      currentStep = 3;
    else if (currentHumanCount >= 4*step && currentHumanCount < 5*step)
      currentStep = 4;
    else if (currentHumanCount >= 5*step && currentHumanCount < 6*step)
      currentStep = 5;
    else if (currentHumanCount >= 6*step)
      currentStep = 6;

    if (currentStep == 6 && (currentCityCount < requiredCityCount))
      currentStep = 5;

    SelectSprite();
  }
}

