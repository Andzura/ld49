using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEditor;
using UnityEngine.SceneManagement;

public class StabilityManager : MonoBehaviour
{
  public Map map;
  private InstabilityJauge instabilityJauge;
  private RepartitionJauge repartitionJauge;
  private LaserJauge laserJauge;

  public float eqIncrement = 0.2f;
  public float eqDecrement = 0.001f;
  public float eqThreshold = 1f;
  private float margin = 0.2f;

  private int forestCount;
  private int oceanCount;
  private int mountainCount;
  private int cityCount;

  private float earthQuakeProb = 0.0010f;
  private float earthQuakeGauge = 0f;
  private int humans = 0;

  private float humanBirthDeltaTimeThreshold = 1;
  private float humanBirthDeltaTime = 0;
  private int humanToSavePlanet = 150;
  private int cityToSavePlanet = 1;

  private bool timeIsOn = false;
  internal float gameTimer = 0;
  internal float gameTotalTime = 300;

  public PlayableDirector impactCutScene;

  private bool cutScenePlayed = false;
  private bool hudDisplayed = false;

// Start is called before the first frame update
void Start()
  {
    GlobalPrefabs.LoadAll();
    forestCount = 0;
    oceanCount = 0;
    mountainCount = 0;
  }

  public void CountTiles() {
    int tmpForest = 0;
    int tmpOcean = 0;
    int tmpMountain = 0;
    int tmpCity = 0;
    foreach(var t in map.tiles){
      switch(t.type){
        case("Forest"):
          tmpForest++;
          break;
        case("Mountain"):
          tmpMountain++;
          break;
        case("Ocean"):
          tmpOcean++;
          break;
        case("City"):
          tmpCity++;
          break;
        default:
          break;
      }
    }
    this.forestCount = tmpForest;
    this.oceanCount = tmpOcean;
    this.mountainCount = tmpMountain;
    this.cityCount = tmpCity;
  }


  void FixedUpdate() {
    if (this.instabilityJauge != null)
      this.instabilityJauge.SetLevel(this.earthQuakeGauge);

    this.CountTiles();
    
    if (this.timeIsOn) {
      this.gameTimer += Time.deltaTime;
      if (this.gameTimer > this.gameTotalTime)
        this.EndGame();
    }

    humanBirthDeltaTime += Time.deltaTime;
    this.humans = map.HumanCount();
//    Debug.Log(humans);
    if (Input.GetKeyDown(KeyCode.Space))
      this.CreateHuman();

    this.HandleEarthQuake();

    if(IsStable() && (this.humanBirthDeltaTime > this.humanBirthDeltaTimeThreshold)) {
      this.humanBirthDeltaTime = 0;
      Debug.Log("ON CREE DES HUMAINS");
      this.CreateHuman();
      /*
      if (!cutScenePlayed)
        StartImpactCutscene();
      */
    }
    int maxCount = this.forestCount + this.oceanCount + this.mountainCount;
    if(maxCount > 9 && !cutScenePlayed){
      StartImpactCutscene();
    }
    if(this.laserJauge != null)
      this.laserJauge.SetCount(this.humans, this.cityCount);

    if(cutScenePlayed && !hudDisplayed && !CutsceneIsPlaying()) {
      hudDisplayed = true;
      DisplayHud();
    }
  }

  private void CreateHuman() {
    Tile choosen;
    int count = 0;
    do {
      choosen = map.tiles[Random.Range(0,map.tiles.Count)];
      count++;
    } while(!IsHabitable(choosen) && count <= map.tiles.Count);
    choosen.HumanIsBorn();
  }

  private void StartImpactCutscene() {
    cutScenePlayed = true;
    impactCutScene.Play();
    LaunchGameTimer();
  }

  private void StartVictoryCutscene() {
    SceneManager.LoadScene("FinGagne", LoadSceneMode.Single);
  }

  private void StartDefeatCutscene() {
    SceneManager.LoadScene("FinPerdu", LoadSceneMode.Single);
  }

  private bool IsHabitable(Tile tile) {
    if(tile.type == "Forest" || tile.type == "Mountain" || tile.type == "Ocean") {
      return (tile.tileComponent.currentEvent == null);
    }
    return false;
  }

  private void HandleEarthQuake() {
    if(earthQuakeGauge >= eqThreshold){
      if(Random.value <= earthQuakeProb){
        earthQuakeGauge = 0f;
        bool ok = false;
        TileType tileType;
        do{
          tileType = this.map.tiles[Random.Range(0,this.map.tiles.Count)].tileComponent;
          ok = (tileType.currentEvent == null);
        }while(!ok);
        tileType.currentEvent = tileType.gameObject.AddComponent<EarthQuake>();
        tileType.currentEvent.tile = tileType.tile;
      }
    }
    this.earthQuakeGauge -= this.eqDecrement;
    if (this.earthQuakeGauge < 0)
      this.earthQuakeGauge = 0;
  }

  public void IncrementEarthQuake(){
    this.earthQuakeGauge += this.eqIncrement;
  }

  public bool IsStable() {
    int maxCount = this.forestCount + this.oceanCount + this.mountainCount;
    if(maxCount > 9) {
      float forestPercent = (float)forestCount/(float)maxCount;
      float mountainPercent = (float)mountainCount/(float)maxCount;
      float oceanPercent = (float)this.oceanCount/(float)maxCount;
      float error = Mathf.Abs((1f/3f) - forestPercent) + Mathf.Abs((1f/3f) - oceanPercent) + Mathf.Abs((1f/3f) - mountainPercent);
      if (this.repartitionJauge != null){
        this.repartitionJauge.SetDelta(error);
        this.repartitionJauge.oceanPercent = oceanPercent;
        this.repartitionJauge.mountainPercent = mountainPercent;
        this.repartitionJauge.forestPercent = forestPercent;
        this.repartitionJauge.RefreshPieChart();
      }
      return error <= this.margin;
    }
    if (this.repartitionJauge != null){
      this.repartitionJauge.SetDelta(2f);
      this.repartitionJauge.RefreshPieChart();
  
    }
    return false;
  }

  public bool CutsceneIsPlaying() {
    if (impactCutScene.state != PlayState.Playing)
      return false;
    return true;
  }

  public void LaunchGameTimer() {
    this.timeIsOn = true;
  }


  // Update is called once per frame
  void Update(){}

  private void EndGame() {
    if ((map.HumanCount() >= this.humanToSavePlanet) && (this.cityCount >= this.cityToSavePlanet))
      StartVictoryCutscene();
    else
      StartDefeatCutscene();

    // While we launch it from unity editor
    //EditorApplication.isPlaying = false;

    // To Use when we will be launching it from build
    //Application.Quit();
    return;
  }

  private void DisplayHud() {
    this.instabilityJauge = Instantiate(Resources.Load<GameObject>("Prefabs/Instability")).GetComponent<InstabilityJauge>();
    this.instabilityJauge.currentLevel = 0;
    this.repartitionJauge = Instantiate(Resources.Load<GameObject>("Prefabs/Repartition")).GetComponent<RepartitionJauge>();
    this.laserJauge = Instantiate(Resources.Load<GameObject>("Prefabs/Laser")).GetComponent<LaserJauge>();
    this.laserJauge.requiredCityCount = cityToSavePlanet;
    this.laserJauge.requiredHumanCount = humanToSavePlanet;
    Instantiate(Resources.Load("Prefabs/ImpactManager"));
  }
}
