using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;

public class Fire : TileEvent {

  public float eventTime = 0;
  public float lastPropagation = 0;
  public float propagationRatio = 0;


  public Fire() {
    eventTime = 10;
    propagationRatio = 0.005f;
    eventOngoing = true;
    type = 0;
  }

  public override void HandleEvent(float deltaTime) {
    if(this.playingMinigame)
      return;
      
    eventTime -= deltaTime;
    lastPropagation += deltaTime;
    if(eventTime <= 0) {
      eventOngoing = false;
      if(this.tile.tileComponent.village){
        this.tile.tileComponent.KillVillage();
      }else{
        this.tile.EventSetType("Sterile");
      }
    }
    else if(lastPropagation > 1) {
      Propagate();
      lastPropagation = 0;
    }
    base.HandleEvent(deltaTime);
  }

  public override void Propagate() {
    this.tile.neighboors.ForEach(neighboor => {
      if ((neighboor.GetCurrentType() != this.tile.GetCurrentType()) || neighboor.tileComponent.currentEvent != null) {
        return;
      }

      if(UnityEngine.Random.value < propagationRatio) {
        propagationRatio = 0.005f;
        neighboor.tileComponent.EventOn();
      } else {
        propagationRatio += 0.005f;
        if (propagationRatio > 1) {
          propagationRatio = 1;
        }
      }

    });
  }

}
