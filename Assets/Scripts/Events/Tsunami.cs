using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;

public class Tsunami : TileEvent {

  public float eventTime = 0;

  public int direction;
  public bool computeDirection = true;

  public Tsunami() {
  eventTime = 5;
  eventOngoing = true;
  type = 0;
}

  private void Start()
  {
  if(computeDirection)
      direction = FindDirection();
  }

  public override void HandleEvent(float deltaTime) {
    if(this.playingMinigame)
      return;

    eventTime -= deltaTime;

    if(eventTime <= 0) {
      Propagate();
      eventOngoing = false;
    }
    base.HandleEvent(deltaTime);
  }


  int FindDirection()
  {
      int initialDirection = UnityEngine.Random.Range(0, 6);
      int tryDirection = initialDirection;
      do
      {
          Tile neighbor = tile.map.GetNeighborInDirection(tile, tryDirection);
          while (neighbor != null)
          {
              if (neighbor.GetCurrentType() != tile.GetCurrentType())
                  break;
              neighbor = tile.map.GetNeighborInDirection(neighbor, tryDirection);
          }
          if (neighbor == null)
          {
              tryDirection = (tryDirection + 1) % 6;
          }
          else
          {
              return tryDirection;
          }
      } while (tryDirection != initialDirection);

      return initialDirection;
  }

  public override void Propagate()
  {
      Tile neighboor = tile.map.GetNeighborInDirection(tile, direction);
      if (neighboor == null)
      {
          this.tile.map.TsunamiIsOver();
          return;
      }
      if(neighboor.GetCurrentType()=="Ocean")
      {
        if (neighboor.tileComponent.village) {
          neighboor.tileComponent.KillVillage();
          this.tile.map.TsunamiIsOver();
          return;
        }
        neighboor.tileComponent.EventOn();
        neighboor.tileComponent.currentEvent.SetOrigin(this.tile);
        ((Tsunami)neighboor.tileComponent.currentEvent).direction = direction;
        ((Tsunami)neighboor.tileComponent.currentEvent).computeDirection = false;
        return;
      }
      else
      {
          Impact(neighboor);
          return;
      }
  }

  public void Impact(Tile impactedTile) {
    Tile left = impactedTile.map.GetNeighborInDirection(impactedTile, (direction + 2) % 6);
    Tile right = impactedTile.map.GetNeighborInDirection(impactedTile, (direction<2?direction+4:direction-2));
    if (left != null && left.GetCurrentType() != "Mountain") {
      if(left.tileComponent.village) {
        left.tileComponent.KillVillage();
      } else {
        left.EventSetType("Ocean");
        left.tileComponent.SetCooldown();
      }
    }
    if (right != null && right.GetCurrentType() != "Mountain")
    {
      if(right.tileComponent.village) {
        right.tileComponent.KillVillage();
      } else {
        right.EventSetType("Ocean");
        right.tileComponent.SetCooldown();
      }
    }
    if (impactedTile != null && impactedTile.GetCurrentType() != "Mountain")
    {
      if(impactedTile.tileComponent.village) {
        impactedTile.tileComponent.KillVillage();
      } else {
        impactedTile.EventSetType("Ocean");
        impactedTile.tileComponent.SetCooldown();
      }
    }

    this.tile.map.TsunamiIsOver();
  }
  public override void ResolveMinigame(bool status){
    if(status){
      this.tile.tileComponent.currentEvent = null;
      this.tile.map.TsunamiIsOver();
      Destroy(this);
      return;
    }
    this.playingMinigame = false;
    this.minigameDone = true;
  }
}
