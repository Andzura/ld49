using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;

public class Avalanche : TileEvent {

  public float eventTime = 0;
  public float propagationRatio = 0.5f;

  public Avalanche() {
    eventTime = 10;
    eventOngoing = true;
    type = 0;
  }

  public override void HandleEvent(float deltaTime) {
    if(this.playingMinigame)
      return;
      
    eventTime -= deltaTime;

    if(eventTime <= 0) {
      eventOngoing = false;
      Propagate();
    }
    base.HandleEvent(deltaTime);
  }

  public override void Propagate() {
    this.tile.neighboors.ForEach(neighboor => {
      if ((neighboor.GetCurrentType() == "Forest") && (UnityEngine.Random.value < propagationRatio)) {
        if (neighboor.tileComponent.village) {
          neighboor.tileComponent.KillVillage();
          return;
        } else {
          neighboor.tileComponent.currentEvent = neighboor.tileComponent.gameObject.AddComponent<Snow>();
          neighboor.tileComponent.currentEvent.tile = neighboor.tileComponent.tile;
          return;
        }
      } else if ((neighboor.GetCurrentType() == "City") && (UnityEngine.Random.value < propagationRatio/2)) {
        neighboor.tileComponent.currentEvent = neighboor.tileComponent.gameObject.AddComponent<Snow>();
        neighboor.tileComponent.currentEvent.tile = neighboor.tileComponent.tile;
        return;
      } else if ((neighboor.GetCurrentType() != "Mountain") && (UnityEngine.Random.value < propagationRatio)) {
        if(neighboor.tileComponent.village) {
          neighboor.tileComponent.KillVillage();
          return;
        }
        return;
      }
    });
  }

}
