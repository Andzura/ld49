using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;

public class Snow : TileEvent {

  public float eventTime = 0;
  public float propagationRatio = 0.5f;

  public Snow() {
    eventTime = 3;
    eventOngoing = true;
    type = 3;
  }

  public override void HandleEvent(float deltaTime) {
    eventTime -= deltaTime;

    if(eventTime <= 0) {
      eventOngoing = false;
      this.tile.EventSetType("Sterile");
      this.tile.tileComponent.SetCooldown();
    }
    base.HandleEvent(deltaTime);
  }

  public override void Propagate() {
  }

}
