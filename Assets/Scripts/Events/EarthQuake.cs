using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using PlanetEvent;

public class EarthQuake : TileEvent {

  public float eventTime = 0;
  public float propagationRatio = 0;
  private UnityEngine.Object warningPrefabs;

  private UnityEngine.Object warning;

  public EarthQuake() {
    warningPrefabs = GlobalPrefabs.getPrefab("EQWarning");
    eventTime = 10;
    this.propagationRatio = 1.0f;
    eventOngoing = true;
    type = 1;
  }

  void Start(){
        Debug.Log("kraboom");
     warning = Instantiate(warningPrefabs, this.gameObject.transform);
  }

  public override void HandleEvent(float deltaTime) {
    eventTime -= deltaTime;

    bool propagated = false;
    bool appeared = false;
    if (eventTime <= 0) {
      eventOngoing = false;
      this.tile.EventSetType("Sterile");
      this.tile.tileComponent.SetCooldown();
    }
    else if (eventTime <= 3 && !propagated)
    {
      Propagate();
      propagated = true;
      ((Volcano)this.tile.tileComponent).isLava = true;
    }
    else if (eventTime <=5 && !appeared)
    { 
      if (this.tile.tileComponent.village) {
        this.tile.tileComponent.KillVillage();
      } else {
        this.tile.EventSetType("Volcano");
        this.tile.tileComponent.currentEvent = this;
        this.tile.tileComponent.SetCooldown();
      }
      Destroy(warning);
      appeared = true;
    }
    base.HandleEvent(deltaTime);
  }

  public override void Propagate() {
    this.tile.neighboors.ForEach(neighboor => {
      if (UnityEngine.Random.value < this.propagationRatio) {
        if (neighboor.tileComponent.village) {
          neighboor.tileComponent.KillVillage();
        } else
        {
          neighboor.tileComponent.currentEvent = neighboor.tileComponent.gameObject.AddComponent<Lava>();
          neighboor.tileComponent.currentEvent.tile = neighboor.tileComponent.tile;
          neighboor.checkBorders();
        }
      }
    });
  }

}
