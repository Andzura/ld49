using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

namespace PlanetEvent {
  public abstract class TileEvent : MonoBehaviour {

    public Tile tile;
    public bool eventOngoing;
    public int type; //0=tile dependent, 1=volcano, 2=lava, 3=snow 

    internal bool minigameDone = false;
    internal bool playingMinigame = false;
    
    public abstract void Propagate();

    public void HandleEvent() {
      return;
    }

    public virtual void HandleEvent(float deltaTime) {
      if(!eventOngoing){
        Destroy(this);
      }
      return;
    }

    public virtual void SetOrigin(Tile tile) {

    }

    public virtual void ResolveMinigame(bool status){
      if(status){
        this.tile.tileComponent.currentEvent = null;
        Destroy(this);
        return;
      }
      this.playingMinigame = false;
      this.minigameDone = true;
    }

    public void startingMinigame(){
      this.playingMinigame = true;
    }
  }
}