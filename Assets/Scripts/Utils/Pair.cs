using UnityEngine;
public class Pair<T>
{
  public T first { get; }
  public T second { get; }

  public Pair(T item1, T item2)
  {
    this.first = item1;
    this.second = item2;
  }

  // override object.Equals
  public override bool Equals(object obj)
  {

    if (obj == null || GetType() != obj.GetType())
    {
      return false;
    }

    Pair<T> p = (Pair<T>)obj;
    if (p.first.Equals(this.first) && p.second.Equals(this.second))
    {
      return true;
    }
    return false;
  }

  public override int GetHashCode(){
    return this.first.GetHashCode() ^ this.second.GetHashCode();
  }

    public static bool operator ==(Pair<T> point1, Pair<T> point2)
  {
    return point1.Equals(point2);
  }

  public static bool operator !=(Pair<T> point1, Pair<T> point2)
  {
    return !point1.Equals(point2);
  }
}