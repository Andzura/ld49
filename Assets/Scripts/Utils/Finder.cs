using System.Collections.Generic;
using UnityEngine;
using System;

public class Finder{
  public static StabilityManager FindManager(){
    GameObject g = GameObject.FindGameObjectWithTag("Manager");
    if(g == null){
      return null;
    }
    return g.GetComponent<StabilityManager>();
  }

  public static Map FindMap(){
    GameObject g = GameObject.FindGameObjectWithTag("Map");
    if(g == null){
      return null;
    }
    return g.GetComponent<Map>();
  }
  public static AudioManagerIntro FindSoundManager()
  {
    GameObject g = GameObject.FindGameObjectWithTag("SoundManager");
    if (g == null)
    {
      return null;
    }
    return g.GetComponent<AudioManagerIntro>();
  }

}