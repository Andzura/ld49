using UnityEngine;

public class PerlinNoiseGenerator{
  private float elapsed = 0f;
  private float y;
  private float speed;

  public PerlinNoiseGenerator(float y, float speed){
    this.y = y;
    this.speed = speed;
  }
  public float generate(float delta){
    elapsed += delta;
    return Mathf.PerlinNoise(elapsed*speed, y);
  }
}