using UnityEngine;
using System.Collections;
using System.Collections.Generic;   //Required for Dictionary

public class GlobalPrefabs : ScriptableObject
{
	public static Dictionary<int, Object> objectList = new Dictionary<int, Object>();

  public static bool loaded = false;
  public static void LoadAll()
	{
		Object[] ObjectArray = Resources.LoadAll("Prefabs");
		foreach (Object o in ObjectArray){
			objectList.Add(o.name.GetHashCode(), (Object)o);
    }
    loaded = true;
	}

  public static Object getPrefab(string objName)
	{
    if(!loaded){
      LoadAll();
    }
		Object obj;

		if (objectList.TryGetValue(objName.GetHashCode(), out obj))
			return obj;
		else
		{
			Debug.Log("Object not found");
			return null;
		}
	}
}