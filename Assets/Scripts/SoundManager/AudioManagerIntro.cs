using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerIntro : MonoBehaviour
{
    private int age = 0;
    public bool fadeOut = false;
    public List<AudioClip> clips;
  public AudioClip minigameClip;
  int currentClip = 0;
  float clipTime = 0f;
  float minigameClipTime = 0f;

  bool inMinigame = false;
  bool leavingMinigame = false;
  // Start is called before the first frame update
  void Start()
    {
        gameObject.GetComponent<AudioSource>().volume = 0f;
        gameObject.GetComponent<AudioSource>().loop = false;
    }

    // Update is called once per frame
  void Update()
  {
    age++;
    if(age<800)
    {
        gameObject.GetComponent<AudioSource>().volume = ((float)age)/800f;
    }
    if(fadeOut && gameObject.GetComponent<AudioSource>().volume>0f)
    {
        float newVolume = gameObject.GetComponent<AudioSource>().volume - (1f / 360f);
        gameObject.GetComponent<AudioSource>().volume = Mathf.Max(newVolume, 0f);
    }
    if(inMinigame && gameObject.GetComponent<AudioSource>().volume==0f)
    {
      gameObject.GetComponent<AudioSource>().clip = minigameClip;
      gameObject.GetComponent<AudioSource>().time = Mathf.Min(minigameClipTime, gameObject.GetComponent<AudioSource>().clip.length - 0.01f);
      gameObject.GetComponent<AudioSource>().Play();
      gameObject.GetComponent<AudioSource>().loop = true;
      age = 1;
      fadeOut = false;
    }
    else if((leavingMinigame && gameObject.GetComponent<AudioSource>().volume == 0f))
    {
      gameObject.GetComponent<AudioSource>().clip = clips[currentClip];
      gameObject.GetComponent<AudioSource>().time = Mathf.Min(clipTime, gameObject.GetComponent<AudioSource>().clip.length - 0.01f);
      gameObject.GetComponent<AudioSource>().Play();
      StartFadeIn();
      leavingMinigame = false;
    }
    if (!gameObject.GetComponent<AudioSource>().isPlaying )
    {
      gameObject.GetComponent<AudioSource>().clip = GetNextClip();
      gameObject.GetComponent<AudioSource>().Play();
    }
  }

  AudioClip GetNextClip()
  {
    if(currentClip+1 < clips.Count)
    {
      currentClip++;
    }
    else
    {
      currentClip = 0;
    }
    return clips[currentClip];
  }


    public void StartFadeOut()
    {
        fadeOut = true;
  }
  public void StartFadeIn()
  {
    fadeOut = false;
    age = 0;
  }


  public void StartMinigame()
  {
    clipTime = gameObject.GetComponent<AudioSource>().time;
    inMinigame = true;
    fadeOut = true;
  }

  public void ExitMinigame()
  {
    fadeOut = true;
    inMinigame = false;
    minigameClipTime = gameObject.GetComponent<AudioSource>().time;
    gameObject.GetComponent<AudioSource>().loop = false;
    leavingMinigame = true;
  }

}
