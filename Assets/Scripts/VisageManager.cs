using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisageManager : MonoBehaviour
{
  public Transform visageImg;
  public float maxSpeed = 0.1f;
  public float radius = 1f;
  public int rateNextPos = 10;
  private Vector2 nextPos;
  private int counter = 0;
  private  float currentSpeed = 0f;
  // Start is called before the first frame update
  private bool follow = false;
  public float changeBehaviour = 10;

  public Sprite baseSpr;
  public Sprite left;
  public Sprite right;
  public Sprite both;
  private SpriteRenderer spriteRenderer;
  private Sprite currentSpr;
  void Start(){
    nextPos = transform.position;
    this.spriteRenderer = this.GetComponentInChildren<SpriteRenderer>();
    this.spriteRenderer.sprite = this.baseSpr;
    this.currentSpr = this.baseSpr;
  }

  void FixedUpdate(){
    Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    if((mousePos - (Vector2)visageImg.position).magnitude <= 0.75f){
      if(mousePos.x <= visageImg.position.x -0.1f){
        this.changeSprite(this.left);
      }else if(mousePos.x >= visageImg.position.x + 0.1f){
        this.changeSprite(this.right);
      }else{
        this.changeSprite(this.both);
      }
    }else{
      this.changeSprite(this.baseSpr);
    }
    if(!follow){
      if(Random.Range(0f, 1000f) < changeBehaviour){
        follow = !follow;
        counter = 0;
      }
      if((nextPos - (Vector2)visageImg.transform.position).magnitude <= maxSpeed){
        counter += Random.Range(0, rateNextPos);
        if(counter >= 200){
          counter = 0;
          Vector2 t = Vector2.up * Random.Range(0.1f, radius);
          t = t.Rotate(Random.Range(0f,360f));
          nextPos = t;
          currentSpeed = Random.Range(0f, maxSpeed);
        }
      }
    }else{
      counter += Random.Range(0, rateNextPos);
      if(counter >= 750){
        follow = !follow;
      }
      nextPos = mousePos;
      if(nextPos.magnitude > radius){
        nextPos = nextPos.normalized * radius;
      }
      currentSpeed = maxSpeed/5;
    }
  }
  // Update is called once per frame
  void Update(){
    if(nextPos != (Vector2)transform.position){
      float diff = Time.deltaTime/Time.fixedDeltaTime;
      Vector2 dir = nextPos - (Vector2)visageImg.transform.position;
      if(dir.magnitude < currentSpeed){
        visageImg.transform.position = nextPos;
      }else{
      visageImg.transform.position += (Vector3)(dir.normalized * currentSpeed * diff);
      }
    }

  }

  public void changeSprite(Sprite spr){
    if(spr != currentSpr){
      this.spriteRenderer.sprite = spr;
      this.currentSpr = spr;
    }
  }
}
