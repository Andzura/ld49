using System.Collections;
using System.Collections.Generic;
using static System.Math;
using UnityEngine;
using System;

public class Map : MonoBehaviour {

  public List<Tile> tiles;
  public int firstRowSize;
  public int rowNumber;

  public GameObject TilePrefab;
  public float tileSize;
  public bool tsunamiOnGoing = false;

  // Start is called before the first frame update
  void Start(){
    int rowSize =  firstRowSize;
    int rowStart = 0;
    float width = (Mathf.Sqrt(3f) * tileSize)/2;
    float height = (3*tileSize)/4;
    for(int i = 0; i < rowNumber; i++){
      int offset = rowSize - firstRowSize;
      for(int j = 0; j < rowSize; j++){
        GameObject tmp = Instantiate(TilePrefab, transform);
        tmp.transform.position += new Vector3(j*width-offset*(width/2), -i*(height));
        Tile tmpTile = tmp.GetComponent<Tile>();
        tiles.Add(tmpTile);
        tmpTile.type = "Sterile";
        tmpTile.map = this;
        tmpTile.rank = j + rowStart  + 1;
      }
      rowStart += rowSize;
      rowSize += (i+1 <=  System.Math.Floor((double)rowNumber/2) ? 1 : -1);
    }
    foreach(var tile in tiles){
      tile.Initialize();
    }

  }

  void FixedUpdate() {
    if(!tsunamiOnGoing) {
      List<Tile> oceanTiles = tiles.FindAll(tile => {
        return ((tile.GetCurrentType() == "Ocean") && !tile.tileComponent.village);
      });

      if(oceanTiles.Count > 0) {
        Tile selectedOcean = oceanTiles[UnityEngine.Random.Range(0, oceanTiles.Count - 1)];
        selectedOcean.tileComponent.CreateEvent();
      }
    }
  }

  internal void EnableMap(){
      foreach(var t in tiles){
        t.GetComponent<Collider2D>().enabled = true;
      }
  }

  internal void DisableMap(){
      foreach(var t in tiles){
        t.GetComponent<Collider2D>().enabled = false;
      }
  }

    public void SwitchSprite()
  {
    transform.Find("Circle").gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("PlaneteCorps4");
  }

  public int HumanCount() {
    int humans = 0;
    tiles.ForEach(tile => {
      humans += tile.humans;
    });
    return humans;
  }


  public void TsunamiIsOn() {
    this.tsunamiOnGoing = true;
  }

  public void TsunamiIsOver() {
    this.tsunamiOnGoing = false;
  }

  // Update is called once per frame
  void Update(){}

  public int[] Coordonates(Tile tile) {
    int rank = tile.rank;
    int rowSize = firstRowSize;
    int rowMax = firstRowSize;
    int row = 1;
    int column = 1;

    while(rank > rowMax) {
      row += 1;
      rowSize = row <= System.Math.Ceiling((double)rowNumber / 2) ? rowSize + 1 : rowSize - 1;
      rowMax = rowMax + rowSize;
    }

    column = rank - (rowMax - rowSize);

    int[] coordonates = new int[2];
    coordonates[0] = row;
    coordonates[1] = column;

    return coordonates;
  }

  public int Rank(int row, int column) {
    int rank = 0;
    int currentRow = 1;
    int rowSize = firstRowSize;

    while(currentRow < row) {
      currentRow += 1;
      rank += rowSize;
      rowSize = currentRow <= System.Math.Ceiling((double)rowNumber / 2) ? rowSize + 1 : rowSize - 1;
    }

    rank += column;

    return rank;
  }

  public List<Tile> Neighboors(Tile tile) {
    if(tile.row == null || tile.column == null) {
      tile.CalculateCoordonates();
    }

    int row = tile.row;
    int column = tile.column;

    List<Tile> neighboors = new List<Tile>();

    neighboors.Add(TileByCoordonates(row, column - 1));
    neighboors.Add(TileByCoordonates(row, column + 1));

    if (row <= System.Math.Floor((double)rowNumber/2)) {
      neighboors.Add(TileByCoordonates(row - 1, column - 1));
      neighboors.Add(TileByCoordonates(row - 1, column));
      neighboors.Add(TileByCoordonates(row + 1, column));
      neighboors.Add(TileByCoordonates(row + 1, column + 1));

    } else if (row == System.Math.Ceiling((double)rowNumber/2)) {
      neighboors.Add(TileByCoordonates(row - 1, column - 1));
      neighboors.Add(TileByCoordonates(row - 1, column));
      neighboors.Add(TileByCoordonates(row + 1, column - 1));
      neighboors.Add(TileByCoordonates(row + 1, column));
    } else {
      neighboors.Add(TileByCoordonates(row - 1, column));
      neighboors.Add(TileByCoordonates(row - 1, column + 1));
      neighboors.Add(TileByCoordonates(row + 1, column - 1));
      neighboors.Add(TileByCoordonates(row + 1, column));
    }

    neighboors.RemoveAll(item => item == null);
    return neighboors;
  }

  public Tile TileByRank(int rank) {
    return tiles[rank - 1];
  }


  public Tile GetNeighborInDirection(Tile caller, int direction)
  {
      if(direction==0)//left neighbor
      {
          if (caller.column == 1)
              return null;
          return TileByCoordonates(caller.row, caller.column - 1);
      }
      else if (direction == 1)
      {
          if (caller.row <= firstRowSize)
          {
              if (caller.column == 1 || caller.row == 1)
                return null;
              return TileByCoordonates(caller.row-1, caller.column - 1);
          }
          else
          {
            return TileByCoordonates(caller.row - 1, caller.column);
          }
      }
      else if (direction == 2)
      {
          if (caller.row <= firstRowSize)
          {
              if (caller.column == GetWidthOfRow(caller.row) || caller.row == 1)
                  return null;
              return TileByCoordonates(caller.row - 1, caller.column);
          }
          else
          {
              return TileByCoordonates(caller.row - 1, caller.column + 1);
          }
      }
      else if (direction == 3)
      {
          if (caller.column == GetWidthOfRow(caller.row))
              return null;
          return TileByCoordonates(caller.row, caller.column + 1);
      }
      else if (direction == 4)
      {
          if (caller.row < firstRowSize)
          {
            if(caller.column == GetWidthOfRow(caller.row))
              return null;
            return TileByCoordonates(caller.row +1, caller.column + 1);
          }
          else
          {
              if (caller.row == rowNumber)
                  return null;
              return TileByCoordonates(caller.row + 1, caller.column);
          }
      }
      else if (direction == 5)
      {
          if (caller.row < firstRowSize)
          {
            return TileByCoordonates(caller.row + 1, caller.column);
          }
          else
          {
              if (caller.row == rowNumber || caller.column == 1)
                  return null;
              return TileByCoordonates(caller.row + 1, caller.column - 1);
          }
      }
      return null;
  }


  public int GetWidthOfRow(int row)
  {
    if(row<=firstRowSize)
    {
      return firstRowSize + row -1;
    }
    else
    {
        return firstRowSize + (rowNumber - row);
    }
  }


  public int GetNeigborDirection(Tile caller, Tile neighbor){
    if(!caller.IsNeighbor(neighbor)){
      return -1;
    }
    if(neighbor.row == caller.row){
      return caller.column > neighbor.column ? 0 : 3;
    }
    int dir = 0;
    if(caller.row <= System.Math.Floor((double)rowNumber/2)){
      dir = (caller.column == neighbor.column) ? 2 : 1;
      dir += (caller.row < neighbor.row) ? 3 : 0;
    }else if(caller.row == System.Math.Ceiling((double)rowNumber/2)){
      if(caller.row < neighbor.row){
        dir = (caller.column == neighbor.column) ? 4 : 5;
      }else{
        dir = (caller.column == neighbor.column) ? 2 : 1;
      }
    }else{
      dir = (caller.column == neighbor.column) ? 1 : 2;
      dir += (caller.row < neighbor.row) ? 3 : 0;
    }
    return dir;
  }

  public Tile TileByCoordonates(int row, int column) {
    if (row < 1 || row > rowNumber || column < 1) {
      return null;
    }

    int rowSize;

    if (row <= System.Math.Ceiling((double)rowNumber / 2)) {
      rowSize = firstRowSize + (row - 1);
    } else {
      rowSize = firstRowSize + (rowNumber - row);
    }

    if(column > rowSize) {
      return null;
    }

    return TileByRank(Rank(row, column));
  }



  public void BurnMeteorTiles()
  {
    List<Tile> burnedTiles = new List<Tile>();
    burnedTiles.Add(TileByCoordonates(4, 1));
    burnedTiles.Add(TileByCoordonates(5, 1));
    burnedTiles.Add(TileByCoordonates(5, 2));
    burnedTiles.Add(TileByCoordonates(6, 1));
    burnedTiles.Add(TileByCoordonates(6, 2));
    burnedTiles.Add(TileByCoordonates(7, 1));
    foreach(Tile tile in burnedTiles)
    {
      tile.tileComponent.currentEvent = tile.tileComponent.gameObject.AddComponent<Lava>();
      tile.tileComponent.currentEvent.tile = tile.tileComponent.tile;
      tile.checkBorders();
    }
  }
}
