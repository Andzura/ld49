using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BiomeChangeButton : MonoBehaviour, IPointerClickHandler
{
    public BiomeChange parent;
    public Tile tile;
    public string targetType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        tile.SetType(targetType);
        parent.Shutdown();
    }
}
