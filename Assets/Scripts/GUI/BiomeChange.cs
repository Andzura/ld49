using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BiomeChange : MonoBehaviour{
  public Vector2 screenPos;

  public Tile parent;

  public float maxDist;

  public List<GameObject> buttonsPrefabs;

  private Transform canvas;
  private float currentDist = 0f;
  private List<Transform> spawnedButtons;

  public BiomeChange(){
    spawnedButtons = new List<Transform>();
  }
  void Start(){
    //canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
  }

  public void Shutdown(){
    foreach(var p in spawnedButtons){
      Destroy(p.gameObject);
    }
    spawnedButtons.Clear();
  }

  public void Startup(){
    currentDist = 0f;
    foreach(var p in buttonsPrefabs){
      GameObject g = Instantiate(p);
      g.transform.SetParent(transform);
      g.transform.position = screenPos;
      BiomeChangeButton b = g.GetComponent<BiomeChangeButton>();
      b.parent = this;
      b.tile = this.parent;
      spawnedButtons.Add(g.transform);
    }
  }

  public void FixedUpdate(){
    if(MinigameManager.GetInstance().inGame){
      this.Shutdown();
    }
    if(spawnedButtons != null){
      List<Vector2> offsets = new List<Vector2>();
      offsets.Add((Vector2.up.Rotate(60)).normalized);
      offsets.Add((Vector2.up.Rotate(-60)).normalized);
      offsets.Add(Vector2.down);
      for(int i = 0; i < spawnedButtons.Count; i++){
          Vector2 t = screenPos + offsets[i] * currentDist;
          spawnedButtons[i].position = t;
      }
      if(currentDist < maxDist){
          currentDist += 1f;
      }
    }
  }
}