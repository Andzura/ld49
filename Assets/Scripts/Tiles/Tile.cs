using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour {

  public List<Tile> neighboors;
  public Map map;
  public int rank;
  public int row;
  public int column;
  public String type;
  public TileType tileComponent;
  public GameObject btnSpawnerPrefab;
  public GameObject borderPrefab;
  public float earthQuakeProb = 0.33f;

  public int humans;

  private List<String> tileTypes = new List<String>() { "Forest", "Mountain", "Ocean", "City", "Sterile", "Volcano" };
  private Dictionary<Pair<int>,GameObject> borders = new Dictionary<Pair<int>,GameObject>();

  // Start is called before the first frame update
  void Start() {}

  private void handleForceChange(String targetType) {
    if(UnityEngine.Random.value <= earthQuakeProb){
      this.tileComponent.currentEvent = this.gameObject.AddComponent<EarthQuake>();
      this.tileComponent.currentEvent.tile = this;
    }else{
      this.type = targetType;
      HandleType();
    }
  }

  // Update is called once per frame
  void Update() {}

  public void Initialize() {
    CalculateCoordonates();
    CollectNeighboors();
    HandleType();
  }

  public void KillAllHumans() {
    this.humans = 0;
  }

  public void HumanIsBorn() {
    this.humans += 1;

    if (this.humans == 5) {
      this.tileComponent.InstallVillage();
    } else if (this.humans == 10) {
      this.EventSetType("City");
    }
  }

  public void SetType(String type) {
    if (tileTypes.IndexOf(type) < 0 ) {
      return;
    }
    if(this.tileComponent.CanBeChanged()){
      StabilityManager manager = Finder.FindManager();
      if(manager != null){
        manager.IncrementEarthQuake();
      }
      this.type = type;
      HandleType();
    }else if(this.tileComponent.CanBeForcedChanged()){
      handleForceChange(type);
    }

  }

  public void EventSetType(String type) {
    if (tileTypes.IndexOf(type) < 0 ) {
      return;
    }
    this.type = type;
    HandleType();
  }

  public String GetCurrentType() {
    return this.type;
  }

  public void HandleType() {
    // Changing type kill humans unless we're building a city
    if(!(this.type == "City"))
      this.KillAllHumans();

    Destroy(this.tileComponent);
    this.tileComponent = this.gameObject.AddComponent(System.Type.GetType(this.type)) as TileType;
    this.tileComponent.DisplayTile();
    this.checkBorders();
  }

  public void CalculateCoordonates() {
    int[] coordonates = map.Coordonates(this);
    this.row = coordonates[0];
    this.column = coordonates[1];
  }

  public void CollectNeighboors() {
    this.neighboors = map.Neighboors(this);
  }

  public void checkBorders() {
    List<Pair<int>> borderNeeded = new List<Pair<int>>();
    bool isLava = IsLava();
    foreach(var n in neighboors){
      if(n.type != this.type){
        if(isLava && n.IsLava())
        {
          this.RemoveBorder(n);
        }
        else
          this.AddBorder(n);
      }else{
        this.RemoveBorder(n);
      }
    }

  }

  public void RemoveBorder(Tile n) {
    if(this.IsNeighbor(n)){
      Pair<int> p = new Pair<int>(n.row - this.row, n.column - this.column);
      if(borders.ContainsKey(p)){
        GameObject g = borders[p];
        borders.Remove(p);
        Destroy(g);
        n.RemoveBorder(this);
      }
    }
  }

  private void AddBorder(Tile n) {
    if(this.IsNeighbor(n)){
    Pair<int> p = new Pair<int>(n.row - this.row, n.column - this.column);
      if(!borders.ContainsKey(p)){
        GameObject g = Instantiate(borderPrefab, this.transform);
        int dir = map.GetNeigborDirection(this, n);
        g.transform.Rotate(new Vector3(0,0,1f), -60f*dir);
        borders.Add(p, g);
        n.AddBorder(this);
      }
    }
  }


  public bool IsNeighbor(Tile n) {
    return neighboors.Contains(n);
  }


  public bool IsLava() {
    if (type == "Volcano")
      return true;
    if (tileComponent.currentEvent == null)
      return false;
    if (tileComponent.currentEvent.type != 2)
      return false;
    return true;
  }

  public void OnMouseDown() {
    if (Finder.FindManager().CutsceneIsPlaying())
      return;
    if (!EventSystem.current.IsPointerOverGameObject() && !MinigameManager.GetInstance().inGame) {
      Vector3 toScreen = Camera.main.WorldToScreenPoint(transform.position);
      //GameObject g = Instantiate(btnSpawnerPrefab);
      //g.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
      BiomeChange b = GameObject.FindGameObjectWithTag("Canvas").GetComponentInChildren<BiomeChange>();
      b.Shutdown();
      if (!tileComponent.CanBeChanged() && !tileComponent.CanBeForcedChanged()) {
        if(tileComponent.currentEvent != null && !tileComponent.currentEvent.minigameDone){
          MinigameManager.GetInstance().StartMinigame(tileComponent.currentEvent);
        }
        return;
      }
      b.screenPos = toScreen;
      b.parent = this;
      b.Startup();
    }
  }
}
