using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ocean : TileType {
  GameObject arrow = null;

  public void Awake() {
    this.sprites.Add("normal", Resources.Load<Sprite>("Tiles/Ocean/Ocean"));
    this.sprites.Add("tsunami", Resources.Load<Sprite>("Tiles/Ocean/TsunamiOcean"));
    this.sprites.Add("lava", Resources.Load<Sprite>("Tiles/Ocean/LavaOcean"));
    this.sprites.Add("village", Resources.Load<Sprite>("Tiles/Ocean/VillageOcean"));
    this.sprites.Add("tsunamiVillage", Resources.Load<Sprite>("Tiles/Ocean/TsunamiVillageOcean"));
    this.sprites.Add("lavaVillage", Resources.Load<Sprite>("Tiles/Ocean/LavaVillageOcean"));

      //arrow
    if(arrow == null)
      arrow = Instantiate<GameObject>((GameObject)GlobalPrefabs.getPrefab("TsunamiArrow"), this.transform);
    arrow.SetActive(false);

  }

  public override void DisplayTile() {
    arrow.SetActive(false);
    if (this.tile == null)
      this.InitTile();
    if(!this.village) {
      if (this.currentEvent == null) {
        this.currentSprite = this.sprites["normal"];
      } else  if (this.currentEvent.type == 0) {//default event
        this.currentSprite = this.sprites["tsunami"];
         arrow.SetActive(true);
        arrow.transform.eulerAngles = new Vector3(0f, 0f, (-60f * (float)((Tsunami)currentEvent).direction));
      } else if (this.currentEvent.type == 2) {//lava event
        this.currentSprite = this.sprites["lava"];
      }
    } else {
      if (this.currentEvent == null) {
        this.currentSprite = this.sprites["village"];
      } else  if (this.currentEvent.type == 0) {//default event
        this.currentSprite = this.sprites["tsunamiVillage"];
      } else if (this.currentEvent.type == 2) {//lava event
        this.currentSprite = this.sprites["lavaVillage"];
      }
    }

    base.DisplayTile();
  }

  public Ocean(){
    this.eventProbability = 0.005f;
  }

  public override void EventOn() {
    this.currentEvent = this.gameObject.AddComponent<Tsunami>();
    currentEvent.tile = this.tile;
  }

  public override void CreateOrManageEvent() {
  }

  public override void FixedUpdate() {
    cooldown -= Time.deltaTime;
    if (cooldown < 0) {
      cooldown = 0;
    }
    ManageEvent();
    DisplayTile();
  }

  public void ManageEvent() {
    if (Finder.FindManager().CutsceneIsPlaying())
      return;
    if (this.currentEvent != null) {
      this.HandleEvent();
    }
  }

  public override void CreateEvent() {
    if ((Random.value < eventProbability) && !MinigameManager.GetInstance().inGame) {
      this.tile.map.TsunamiIsOn();
      this.EventOn();
    }
  }

}