using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlanetEvent;

public abstract class TileType : MonoBehaviour {

  public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
  public Sprite currentSprite;

  public bool village = false;

  public TileEvent currentEvent;
  public float eventProbability;

  public float cooldown = 0;

  public Tile tile;

  public void Start() {
    this.tile = this.gameObject.GetComponent<Tile>();
  }

  public void InitTile()
  { this.tile = this.gameObject.GetComponent<Tile>(); }

  public virtual void DisplayTile() {
    gameObject.GetComponent<SpriteRenderer>().sprite = this.currentSprite;
  }

  public virtual void FixedUpdate() {
    cooldown -= Time.deltaTime;
    if (cooldown < 0) {
      cooldown = 0;
    }
    CreateOrManageEvent();
    DisplayTile();
  }

  public void InstallVillage() {
    this.village = true;
  }

  public void KillVillage() {
    this.village = false;
  }

  public virtual bool CanBeChanged() {
    if (cooldown > 0 ) {
      return false;
    }

    if (currentEvent == null) {
      return true;
    }
    return false;
  }

  public virtual bool CanBeForcedChanged(){
    if (currentEvent != null) {
      return false;
    }
    return true;
  }




  public virtual void CreateOrManageEvent() {
    if (this.currentEvent != null) {
      this.HandleEvent();
    } else if (Random.value < eventProbability && !MinigameManager.GetInstance().inGame) {
      this.EventOn();
    }
  }

  public virtual void CreateEvent() {

  }

  public void SetCooldown() {
    cooldown = 10;
  }


  public abstract void EventOn();
  
  public virtual void HandleEvent(){
    if (Finder.FindManager().CutsceneIsPlaying())
      return;
    this.currentEvent.HandleEvent(Time.deltaTime);
  }

}