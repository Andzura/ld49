using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mountain : TileType {

  public void Awake() {
    this.sprites.Add("normal", Resources.Load<Sprite>("Tiles/Mountain/Mountain"));
    this.sprites.Add("avalanche", Resources.Load<Sprite>("Tiles/Mountain/AvalancheMountain"));
    this.sprites.Add("lava", Resources.Load<Sprite>("Tiles/Mountain/LavaMountain"));
    this.sprites.Add("village", Resources.Load<Sprite>("Tiles/Mountain/VillageMountain"));
    this.sprites.Add("avalancheVillage", Resources.Load<Sprite>("Tiles/Mountain/AvalancheVillageMountain"));
    this.sprites.Add("lavaVillage", Resources.Load<Sprite>("Tiles/Mountain/LavaVillageMountain"));
  }

  public override void DisplayTile() {
    if (this.tile == null)
      this.InitTile();
    if(!this.village) {
      if (this.currentEvent == null) {
        this.currentSprite = this.sprites["normal"];
      } else if (this.currentEvent.type == 0) {//default event
        this.currentSprite = this.sprites["avalanche"];
      } else if (this.currentEvent.type == 2) {//lava event
        this.currentSprite = this.sprites["lava"];
      }
    } else {
      if (this.currentEvent == null) {
        this.currentSprite = this.sprites["village"];
      } else if (this.currentEvent.type == 0) {//default event
        this.currentSprite = this.sprites["avalancheVillage"];
      } else if (this.currentEvent.type == 2) {//lava event
        this.currentSprite = this.sprites["lavaVillage"];
      }
    }
    base.DisplayTile();
  }

  public Mountain() {
    this.eventProbability = 0.0001f;
  }

  public override void EventOn() {
    this.currentEvent = this.gameObject.AddComponent<Avalanche>();
    currentEvent.tile = this.tile;
  }

  /*
  public override void HandleEvent() {
    this.currentEvent.HandleEvent(Time.deltaTime);

    if (!currentEvent.eventOngoing) {
      Destroy(this.currentEvent);
      this.currentEvent = null;
      return;
    }
  }
  */
}
