using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volcano : TileType {

  public bool isLava = false;

  public void Awake() {
    this.sprites.Add("normal", Resources.Load<Sprite>("Tiles/Volcano/Volcano"));
    this.sprites.Add("lava", Resources.Load<Sprite>("Tiles/Volcano/LavaVolcano"));
  }

  public override void DisplayTile() {
    if(isLava)
      this.currentSprite = this.sprites["lava"];
    else
      this.currentSprite = this.sprites["normal"];
    base.DisplayTile();
  }

  public override void EventOn() {
    return;
  }

  /*
  public override void HandleEvent() {
    return;
  }
 
  public override void CreateOrManageEvent() {
    return;
  }
  */
  
  public override bool CanBeForcedChanged(){
    return this.CanBeChanged();
  }
}
