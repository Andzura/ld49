using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sterile : TileType {

  public void Awake() {
    this.sprites.Add("fertile", Resources.Load<Sprite>("Tiles/Sterile/Fertile"));
    this.sprites.Add("sterile", Resources.Load<Sprite>("Tiles/Sterile/Sterile"));
    this.sprites.Add("lavaFertile", Resources.Load<Sprite>("Tiles/Sterile/LavaFertile"));
    this.sprites.Add("lavaSterile", Resources.Load<Sprite>("Tiles/Sterile/LavaSterile"));
  }

  public override void DisplayTile() {
    if (this.currentEvent != null && this.currentEvent.type == 2) { //lava
      this.currentSprite = this.sprites["lavaFertile"];
    } else if(cooldown>0f){//sterile
      this.currentSprite = this.sprites["sterile"];
    } else {
      this.currentSprite = this.sprites["fertile"];
    }
    base.DisplayTile();
  }

  public override void EventOn() {
    return;
  }

  /*
  public override void HandleEvent() {
    return;
  }

  public override void CreateOrManageEvent() {
    return;
  }
  */

  public override bool CanBeForcedChanged(){
    return this.CanBeChanged();
  }
}
