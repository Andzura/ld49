using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class City : TileType {

  public void Awake()
  {
    this.sprites.Add("normal", Resources.Load<Sprite>("Tiles/City/City"));
    this.sprites.Add("lava", Resources.Load<Sprite>("Tiles/City/LavaCity"));
    this.sprites.Add("snow", Resources.Load<Sprite>("Tiles/City/SnowCity"));
  }

  public City()
  {
  }

  public override void DisplayTile()
  {
    if (this.tile == null)
      this.InitTile();
    if (this.currentEvent == null)
    {
        this.currentSprite = this.sprites["normal"];
    }
    else if (this.currentEvent.type == 2)
    {//lava event
        this.currentSprite = this.sprites["lava"];
    }
    else if (this.currentEvent.type == 3)
    {//snow event
        this.currentSprite = this.sprites["snow"];
    }
    base.DisplayTile();
  }

  public override void EventOn() {

  }

  public override void CreateOrManageEvent()
  {
    if (Finder.FindManager().CutsceneIsPlaying())
      return;
    if (this.currentEvent != null)
    {
      this.HandleEvent();
    }
  }
}
