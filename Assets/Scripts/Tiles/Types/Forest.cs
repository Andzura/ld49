using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forest : TileType {

  public void Awake() {
    this.sprites.Add("normal", Resources.Load<Sprite>("Tiles/Forest/Forest"));
    this.sprites.Add("burning", Resources.Load<Sprite>("Tiles/Forest/BurningForest"));
    this.sprites.Add("lava", Resources.Load<Sprite>("Tiles/Forest/LavaForest"));
    this.sprites.Add("villagelava", Resources.Load<Sprite>("Tiles/Forest/LavaVillageForest"));
    this.sprites.Add("snow", Resources.Load<Sprite>("Tiles/Forest/SnowForest"));
    this.sprites.Add("village", Resources.Load<Sprite>("Tiles/Forest/VillageForest"));
    this.sprites.Add("burningVillage", Resources.Load<Sprite>("Tiles/Forest/BurningVillageForest"));
    this.sprites.Add("lavaVillage", Resources.Load<Sprite>("Tiles/Forest/LavaVillageForest"));
    this.sprites.Add("snowVillage", Resources.Load<Sprite>("Tiles/Forest/SnowVillageForest"));
  }

  public Forest() {
    this.eventProbability = 0.0001f;
  }

  public override void DisplayTile() {
    if (this.tile == null)
      this.InitTile();
    if(!this.village) {
      if (this.currentEvent == null) {
        this.currentSprite = this.sprites["normal"];
      } else if (this.currentEvent.type == 0) {//default event
        this.currentSprite = this.sprites["burning"];
      } else if (this.currentEvent.type == 2) {//lava event
        this.currentSprite = this.sprites["lava"];
      } else if (this.currentEvent.type == 3) {//snow event
        this.currentSprite = this.sprites["snow"];
      }
    } else {
      if (this.currentEvent == null) {
        this.currentSprite = this.sprites["village"];
      } else if (this.currentEvent.type == 0) {//default event
        this.currentSprite = this.sprites["urningVillage"];
      } else if (this.currentEvent.type == 2) {//lava event
        this.currentSprite = this.sprites["lavaVillage"];
      } else if (this.currentEvent.type == 3) {//snow event
        this.currentSprite = this.sprites["snowVillage"];
      }
    }
    base.DisplayTile();
  }

  public override void EventOn() {
    if(this.village) {
      KillVillage();
    } else {
      this.currentEvent = this.gameObject.AddComponent<Fire>();
      currentEvent.tile = this.tile;
    }
  }

    public override void CreateOrManageEvent() {
    if (Finder.FindManager().CutsceneIsPlaying())
      return;
    if (this.currentEvent != null) {
      this.HandleEvent();
    } else if (!this.village && (Random.value < eventProbability) && !MinigameManager.GetInstance().inGame) {
      this.EventOn();
    }
  }

  /*
  public override void HandleEvent() {
    this.currentEvent.HandleEvent(Time.deltaTime);

    if (!currentEvent.eventOngoing) {
      Destroy(this.currentEvent);
      this.currentEvent = null;
      this.tile.SetType("Sterile");
      this.tile.tileComponent.SetCooldown();
      return;
    }
  }
  */

}
