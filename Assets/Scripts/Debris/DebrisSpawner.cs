using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisSpawner : MonoBehaviour
{
    public int rate = 50;
    public float maxVelocity;
    public GameObject debrisPrefab;
    public PlayerGravity playerGravity;
    private int counter = 0;
    private List<Debris> debrisList;

    // Start is called before the first frame update
    void Start()
    {
        debrisList = new List<Debris>();
    }

    void FixedUpdate(){
        counter += Random.Range(0,rate);
        if (counter >= 100)
            //if (debrisList.Count<1)
        {
            counter = 0;
            GameObject tmpObj = Instantiate(debrisPrefab);
            Vector3 bounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
            float xpos = Random.Range(-2f*bounds.x, -bounds.x);
            float ypos = Random.Range(-1.25f*bounds.y, 1.25f*bounds.y);
            float speed = Random.Range(0.1f, maxVelocity);
            float angle = Random.Range(-5f, 15f);
            float cos = Mathf.Cos(angle * Mathf.Deg2Rad);
            float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
            Vector3 v = new Vector3(speed*cos, speed*sin, 0f);
            tmpObj.GetComponent<Debris>().SetVelocity(v);
            tmpObj.transform.position = new Vector3(xpos, ypos, 0f);
            tmpObj.GetComponent<Debris>().player = playerGravity;
            tmpObj.GetComponent<Debris>().maxVelocity = maxVelocity;
            debrisList.Add(tmpObj.GetComponent<Debris>());
        }
        UpdateDebris();
    }
    // Update is called once per frame
    void Update(){
       
    }


    void UpdateDebris()
    {
        if (debrisList.Count <= 0)
            return;
        for(int i = debrisList.Count-1; i>=0; i--)
        {
            Debris debris = debrisList[i];
            float minDist = playerGravity.GetHorizon();
            float attraction = playerGravity.attractionForce;
            float attractDistance = playerGravity.size;
            Vector3 direction = (playerGravity.transform.position - debris.transform.position);
            float dist = direction.magnitude;
            if (dist <= attractDistance && dist !=0 )
            {
                debris.ApplyForce(direction * (attraction * /*playerGravity.getMass() * */debris.getMass()) / (Mathf.Sqrt(Mathf.Sqrt(dist))));
            }
            if(dist <= minDist)
            {
                playerGravity.Inflate(debris);
                debrisList.RemoveAt(i);
            }
        }
    }
}
