using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour
{
    public float inertia = 0.1f;
    private Vector3 velocity = new Vector3(0f, 0f, 0f);
    private Vector3 acceleration = new Vector3(0f, 0f, 0f);
    private Vector3 force = new Vector3(0f, 0f, 0f);
    public float maxAcceleration = 1f;
    public float maxVelocity;
    public PlayerGravity player;
    //random when spawned?
    private float mass=1f;
    public Vector3 targetPoint = new Vector3(0f, 0f, 0f);
    public int birthDate = 0;
    public int debrisType = 0;
    public Sprite[] spriteArray;

    void Awake(){
    }
    // Start is called before the first frame update
    void Start(){
        this.mass = Random.Range(0.05f,0.6f); 
        this.transform.localScale = new Vector3(mass*0.4f, mass*0.4f,1f);
        debrisType = Random.Range(0, 4);
        gameObject.GetComponent<SpriteRenderer>().sprite = spriteArray[debrisType];
    }

    void FixedUpdate(){
        velocity += acceleration;
        velocity.z = 0f;
        if (velocity.magnitude > maxVelocity)
        {
            velocity = velocity.normalized * maxVelocity;
        }
        transform.position += velocity;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }

    public void ApplyForce(Vector3 addForce)
    {
        force = addForce / mass;
        force.z = 0f;
        acceleration += force;
        acceleration.z = 0f;
        if(acceleration.magnitude>maxAcceleration)
        {
            acceleration = acceleration.normalized * maxAcceleration;
        }
    }

    public void SetVelocity(Vector3 v)
    {
        velocity = v;
    }

    public void SetAcceleration(Vector3 a)
    {
        acceleration = a;
    }

    // Update is called once per frame
    void Update(){
       
    }


    public float getMass()
    {
        return mass;
    }
}
