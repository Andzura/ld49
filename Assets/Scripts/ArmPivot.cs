using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmPivot : MonoBehaviour
{
    float elapsed = 0f;
    private PerlinNoiseGenerator perlinNoiseGenerator;
    // Start is called before the first frame update
    void Start(){
      this.perlinNoiseGenerator = new PerlinNoiseGenerator(200f, 0.1f);  
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        float t = perlinNoiseGenerator.generate(Time.deltaTime);
        float angle = -t*40f;

        gameObject.transform.eulerAngles = new Vector3(0f, 0f, angle);
    }
}
