using UnityEngine;

public class Block : MonoBehaviour {
  public Vector2 movementAxis;
  public Vector2 startPos;
  public Collider2D collider2D;
  public Rigidbody2D rb;
  void Start(){
    this.movementAxis.Normalize();
    this.startPos = transform.position;
    Debug.Log(transform.rotation);
    this.rb = GetComponent<Rigidbody2D>();
    this.collider2D = GetComponent<Collider2D>();
  }
  
  public void OnMouseDrag() {
    Vector2 pos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.startPos;
    Vector2 next = this.startPos+(Vector2.Dot(pos, this.movementAxis) * this.movementAxis);
    Vector2 dir = next - (Vector2)transform.position;
    RaycastHit2D[] hit = new RaycastHit2D[1];
    ContactFilter2D cf = new ContactFilter2D();
    cf.SetLayerMask(LayerMask.GetMask("Minigames"));
    if(rb.Cast(dir.normalized,cf,hit,dir.magnitude) > 0){
      float mag = hit[0].distance-0.01f;
     
      rb.MovePosition((Vector2)transform.position+dir.normalized*mag);
      return;
    }
    rb.MovePosition((Vector2)transform.position+dir);   
  }
}