using UnityEngine;

public class WinningBlock : Block {
  private float winningDistance = 3f;
  private Minigame mg;
  void FixedUpdate(){
    if((startPos - (Vector2)transform.position).magnitude >= winningDistance){
      this.mg.Win();
    }
  }

  public void SetMinigame(Minigame mg){
    this.mg = mg;
  }
}