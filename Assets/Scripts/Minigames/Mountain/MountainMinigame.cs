using UnityEngine;

public class MountainMinigame : Minigame {
  private GameObject level;
  public Sprite[] sprites;
  public Sprite[] sprites_win;
  public Sprite[] sprites_fail;
  private float duration;
  private bool endCs;
  void Start(){
    endCs = false;
    duration = timer;
    string[] levels = {"MountainLevel1", "MountainLevel2", "MountainLevel3"};
    this.level = Instantiate((GameObject)GlobalPrefabs.getPrefab(levels[Random.Range(0, levels.Length)]), transform);
    level.GetComponentInChildren<WinningBlock>().SetMinigame(this);
  }

  public override void Update(){
    //base.Update();
     timer -= Time.deltaTime;
    if(timer < 0){
      if(endCs){
        MinigameManager.GetInstance().ResolveMinigame(status);
      }else{
        endCs = true;
        timer = 2f;
        duration = timer;
        Destroy(level);
      }
    }

    if(!endCs){
      gameObject.GetComponent<SpriteRenderer>().sprite = sprites[Mathf.FloorToInt((sprites.Length) * ((duration-timer)/duration))];
    }else if(status){
      gameObject.GetComponent<SpriteRenderer>().sprite = sprites_win[Mathf.FloorToInt((sprites_win.Length-1) * ((duration-timer)/duration))];
    }else{
      gameObject.GetComponent<SpriteRenderer>().sprite = sprites_fail[Mathf.FloorToInt((sprites_fail.Length-1) * ((duration-timer)/duration))];

    }
  }

  public override void Win(){
    this.status = true;
    this.endCs = true;
    this.timer = 1.5f;
    this.duration = timer;
    Destroy(level);
    //base.Win();
  }
  
}