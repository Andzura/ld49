using UnityEngine;

public class Minigame : MonoBehaviour {
  public float timer;

  protected bool status = false;

  void Start(){
    this.status = false;
  }
  public virtual void Update(){
    timer -= Time.deltaTime;
    if(timer < 0){
      MinigameManager.GetInstance().ResolveMinigame(status);
    }
  }

  public virtual void Win(){
    this.status = true;
    MinigameManager.GetInstance().ResolveMinigame(status);
  }

  public virtual void Loose(){
    this.status = false;
    MinigameManager.GetInstance().ResolveMinigame(status);
  }
}