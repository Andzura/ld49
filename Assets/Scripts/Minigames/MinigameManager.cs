using System.Collections;
using System.Collections.Generic;
using PlanetEvent;
using UnityEngine;

public class MinigameManager {
  private static MinigameManager instance = null;
  internal bool inGame = false;
  private TileEvent target;
  private Object currentMinigame = null;

  public MinigameManager(){
  }

  public static MinigameManager GetInstance(){
    if(instance == null){
      instance = new MinigameManager();
    }
    return instance;
  }

  public void StartMinigame(TileEvent e){
    this.inGame = true;
    this.target = e;
    
    this.target.tile.map.DisableMap();
    //spawn Minigame
    Object minigame;
    switch(this.target.tile.type){
      case("Forest"): 
        minigame = GlobalPrefabs.getPrefab("Forest_Minigame");
        break;
      case("Mountain"):
        minigame = GlobalPrefabs.getPrefab("Mountain_Minigame");
        break;
      case("Ocean"):
        minigame = GlobalPrefabs.getPrefab("Ocean_Minigame");
        break;
      default:
        minigame = null;
        break;
    }
    if(minigame != null){
      this.target.startingMinigame();
      this.currentMinigame = GameObject.Instantiate(minigame);
    }

    //start minigame audio
    Finder.FindSoundManager().StartMinigame();
  }
  
  public void ResolveMinigame(bool status){
    if(this.inGame){
      this.inGame = false;
      this.target.tile.map.EnableMap();
      this.target.ResolveMinigame(status);
      GameObject.Destroy(this.currentMinigame);
      Finder.FindSoundManager().ExitMinigame();
    }
  }
}