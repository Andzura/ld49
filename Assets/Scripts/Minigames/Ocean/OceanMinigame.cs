using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OceanMinigame : Minigame {
  private Vector3 startPos;
  public float duration = 10f;
  public float ratio = 0f;
  public float increment = 0.05f;
  public TsunamiSprites tsunami;
  public Sprite fullbackSprite;
  bool lost = false;
  bool won = false;
  public Sprite[] backgrounds;

  void Start()
  {
    timer = duration;
    ratio = 0.2f;
  }

  public override void Update(){
    if(won)
    {
      if (timer <= 0)
      {
        this.status = true;
        MinigameManager.GetInstance().ResolveMinigame(status);
      }
      else
        tsunami.SetIthSprite(Mathf.FloorToInt(3f-(3f-timer)));
    }
    else if(lost)
    {
      if (timer <= 0)
      {
        this.status = false;
        MinigameManager.GetInstance().ResolveMinigame(status);
      }
      else
        tsunami.SetIthSprite(Mathf.FloorToInt(14f + (3f - timer)));
    }
    else
    {
      if (Input.GetMouseButtonDown(0))
      {
        this.startPos = Input.mousePosition;
      }
      else if (Input.GetMouseButtonUp(0))
      {
        if (this.startPos.x < Input.mousePosition.x)
        {
          ratio -= increment;
          gameObject.GetComponent<Animator>().Play("Ocean_bg_spin");
        }
      }
      if (ratio <= 0f)
      {
        this.Win();
      }
      ratio += Time.deltaTime / 7f;
      ratio = Mathf.Min(ratio, 1f);
      tsunami.SetSprite(ratio);
      if(timer<=0)
      {
        this.Loose();
      }
    }

    timer -= Time.deltaTime;
  }


  public override void Loose()
  {
    timer = 3f;
    lost = true;
    gameObject.GetComponent<Animator>().Play("End");
  }
  public override void Win()
  {
    timer = 3f;
    won = true;
    gameObject.GetComponent<SpriteRenderer>().sprite = fullbackSprite;
    gameObject.GetComponent<Animator>().Play("End");
  }

}