using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
  public int rate = 50;
  public float maxVelocity;
  public GameObject cloudPrefab;
  public PlayerCloud playerCloud;
  private int counter = 0;
  private List<Cloud> cloudList;

  // Start is called before the first frame update
  void Start()
  {
    cloudList = new List<Cloud>();
  }

  void FixedUpdate()
  {
    counter += Random.Range(0, rate);
    if (counter >= 100)
    {
      counter = 0;
      GameObject tmpObj = Instantiate(cloudPrefab);
      Vector3 bounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
      float xpos = Random.Range(-2f * bounds.x, -bounds.x);
      float ypos = Random.Range(0f, 0.8f * bounds.y);
      float speed = Random.Range(0.02f, maxVelocity);
      float angle = Random.Range(-3f, 3f);
      float cos = Mathf.Cos(angle * Mathf.Deg2Rad);
      float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
      Vector3 v = new Vector3(speed * cos, speed * sin, 0f);
      tmpObj.GetComponent<Cloud>().SetVelocity(v);
      tmpObj.transform.position = new Vector3(xpos, ypos, 0f);
      tmpObj.GetComponent<Cloud>().player = playerCloud;
      tmpObj.GetComponent<Cloud>().maxVelocity = maxVelocity;
      cloudList.Add(tmpObj.GetComponent<Cloud>());
    }
    UpdateClouds();
  }
  // Update is called once per frame
  void Update()
  {

  }


  void UpdateClouds()
  {
    if (cloudList.Count <= 0)
      return;
    for (int i = cloudList.Count - 1; i >= 0; i--)
    {
      Cloud cloud = cloudList[i];
      float minDist = playerCloud.GetHorizon();
      float attraction = playerCloud.attractionForce;
      float attractDistance = playerCloud.size;
      Vector3 direction = (playerCloud.transform.position - cloud.transform.position);
      float dist = direction.magnitude;
      if (dist <= attractDistance && dist != 0)
      {
        cloud.ApplyForce(direction * (attraction * /*playerGravity.getMass() * */cloud.getMass()) / (Mathf.Sqrt(Mathf.Sqrt(dist))));
      }
      if (dist <= minDist)
      {
        playerCloud.Inflate();
        cloudList.RemoveAt(i);
        Destroy(cloud.gameObject);
      }
    }
  }


  public void Lose()
  {
    rate = 0;
    for (int i = cloudList.Count - 1; i >= 0; i--)
    {
      Cloud cloud = cloudList[i];
      cloudList.RemoveAt(i);
      Destroy(cloud.gameObject);
    }

    Destroy(playerCloud.gameObject);
  }
}
