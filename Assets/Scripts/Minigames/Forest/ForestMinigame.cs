using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ForestMinigame : Minigame {
  private int counter = 0;
  public int goal = 5;
  public float duration = 15f;
  public Sprite[] spriteArray;
  public Sprite[] lostSprites;
  public CloudSpawner spawner;
  public GameObject raincloud;

  bool lost = false;
  bool won = false;

  void Start()
  {
    timer = duration;
    raincloud.SetActive(false);
  }

  public override void Update()
  {
    timer -= Time.deltaTime;
    if (lost)
    {
      if (timer <= 0)
      {
        this.status = false;
        MinigameManager.GetInstance().ResolveMinigame(status);
      }
      else
      {
        if (((int)(timer * 3)) % 3 == 0)
          gameObject.GetComponent<SpriteRenderer>().sprite = lostSprites[0];
        else if (((int)(timer * 3)) % 3 == 1)
          gameObject.GetComponent<SpriteRenderer>().sprite = lostSprites[1];
        else if (((int)(timer * 3)) % 3 == 2)
          gameObject.GetComponent<SpriteRenderer>().sprite = lostSprites[2];
      }
    }
    else if(won)
    {
      if (timer <= 0)
      {
        this.status = true;
        MinigameManager.GetInstance().ResolveMinigame(status);
      }
      else
        gameObject.GetComponent<SpriteRenderer>().sprite = spriteArray[Mathf.FloorToInt((spriteArray.Length - 1) * (timer / 3f))];
    }
    else
    {
      gameObject.GetComponent<SpriteRenderer>().sprite = spriteArray[Mathf.FloorToInt((spriteArray.Length - 2) * ((duration - timer) / duration))+1];
      if (timer <= 0f)
      {
        Loose();
      }
      else if(spawner.playerCloud.Won())
      {
        Win();
      }
    }
  }

  public override void Loose()
  {
    timer = 3f;
    lost = true;
    spawner.Lose();
  }
  public override void Win()
  {
    timer = 3f;
    won = true;
    spawner.Lose();
    raincloud.SetActive(true);
  }
}