using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCloud : MonoBehaviour
{
  public float size = 0.1f;
  public float attractionForce = 0.1f;
  public float growthRate = 0.1f;
  public float horizon = 0.5f;
  private float mass = 10f;
  public int maxClouds = 150;
  private int counter = 0;
  public CloudSpawner spawner;
  public SpriteRenderer cloudSprite;

  void Start()
  {
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    if (counter>maxClouds)
    {
      spawner.rate = 0;
    }
  }

  public float GetHorizon()
  {
    return horizon;
  }

  public float getMass()
  {
    return mass;
  }

  public void Inflate()
  {
    transform.localScale += new Vector3(growthRate * 0.7f, growthRate * 0.7f);
    horizon += growthRate * 0.7f;
    size += growthRate;
    this.mass += growthRate;
    counter++;
  }


  public bool Won()
  {
    if (counter > maxClouds)
      return true;
    return false;
  }

  void UpdateDrops()
  {
    //if (capturedDebris.Count <= 0)
    //  return;
    //foreach (Debris debris in capturedDebris)
    //{
    //  debris.maxVelocity = 5f;
    //  debris.SetAcceleration(new Vector3(0f, 0f, 0f));

    //  Vector3 rotatedTarget = new Vector3(debris.targetPoint.x * Mathf.Cos((Time.frameCount - debris.birthDate) * rotationSpeed), debris.targetPoint.y * Mathf.Sin((Time.frameCount - debris.birthDate) * rotationSpeed), 0f);

    //  //alt version
    //  //float cos = Mathf.Cos((Time.frameCount - debris.birthDate) * rotationSpeed);
    //  //float sin = Mathf.Sin((Time.frameCount - debris.birthDate) * rotationSpeed);
    //  //Vector3 rotatedTarget = new Vector3(debris.targetPoint.x * cos - debris.targetPoint.y * sin, debris.targetPoint.x * sin + debris.targetPoint.y * cos, 0f);

    //  debris.SetVelocity((transform.position + rotatedTarget - debris.transform.position) * 0.1f);
    //}

    //counter += Random.Range(0, throwawayRate);
    //if (counter >= 100)
    //{
    //  counter = 0;
    //  int rnd = Random.Range(0, capturedDebris.Count);
    //  Debris thrownDebris = capturedDebris[rnd];
    //  capturedDebris.RemoveAt(rnd);
    //}
  }
}
