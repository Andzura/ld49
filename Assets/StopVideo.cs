using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopVideo : MonoBehaviour
{
    public string path;
    // Start is called before the first frame update
    void Start(){
      UnityEngine.Video.VideoPlayer videoPlayer = GetComponent<UnityEngine.Video.VideoPlayer>();
      videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath,path);
      videoPlayer.Play();
    }

    // Update is called once per frame
    void Update(){
        if (Input.GetKey("escape") || !GetComponent<UnityEngine.Video.VideoPlayer>().isPlaying){
            #if UNITY_STANDALONE
                  //Quit the application
                  Application.Quit();
              #endif
          
                  //If we are running in the editor
              #if UNITY_EDITOR
                  //Stop playing the scene
                  UnityEditor.EditorApplication.isPlaying = false;
              #endif
        }
    }
}
